# ics-ans-role-junos-inventory

This Ansible role displays junos inventory information.
Junos roles rely on Juniper specific Ansible specific modules rather rather than the get_facts module.

## Role Variables

```yaml
none
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-inventory
```

## License

BSD 2-clause
